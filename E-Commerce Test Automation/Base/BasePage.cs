﻿using E_Commerce_Test_Automation.Helper;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Configuration;

namespace E_Commerce_Test_Automation.Base
{
    public class BasePage
    {
        protected WebDriverWait wait { get; private set; }
        protected IWebDriver driver { get; private set; }

        public BasePage()
        {
            PageFactory.InitElements(DriverFactory.Instance, this);
            wait = new WebDriverWait(DriverFactory.Instance, TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["DefaultTimeout"])));
            driver = DriverFactory.Instance;
        }

        public void NavigateTo(string url)
        {
            DriverFactory.Instance.Navigate().GoToUrl(url);
        }
    }
}
