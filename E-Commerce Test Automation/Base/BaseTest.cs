﻿using E_Commerce_Test_Automation.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Configuration;

namespace E_Commerce_Test_Automation.Base
{
    public class BaseTest
    {
        protected IWebDriver _driver;

        [SetUp]
        public void SetupTest()
        {
            _driver = new DriverFactory().Initialize(ConfigurationManager.AppSettings["DefaultBrowser"]);
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["URL"]);

            Console.WriteLine(TestContext.CurrentContext.Test.FullName);
        }

        [TearDown]
        public void TearDownTest()
        {
            _driver.Quit();
            _driver = null;
        }
    }
}
