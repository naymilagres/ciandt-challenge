﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Configuration;

namespace E_Commerce_Test_Automation.Helper
{
    public class DriverFactory
    {
        public static IWebDriver Instance { get; set; }

        public static string Url { get; set; }

        public DriverFactory()
        {
            Instance = null;
        }

        public IWebDriver Initialize(string browser)
        {
            double timeout = Convert.ToDouble(ConfigurationManager.AppSettings["DefaultTimeout"]);

            if (Instance == null)
            {
                Instance = BuildLocal();
            }

            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeout);
            Instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(timeout);

            Url = ConfigurationManager.AppSettings["URL"];

            return Instance;
        }

        public static ChromeDriver BuildLocal()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddUserProfilePreference("e.default_directory", AppDomain.CurrentDomain.BaseDirectory.Replace("bin\\Debug\\", "Downloads"));
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

            return new ChromeDriver(chromeOptions);
        }
    }
}
