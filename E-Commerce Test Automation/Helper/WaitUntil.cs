﻿using E_Commerce_Test_Automation.Helper;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;
using System.Threading;

namespace E_Commerce_Test_Automation.Helper
{
    public class WaitUntil
    {
        static double TIMEOUT = Convert.ToDouble(ConfigurationManager.AppSettings["DefaultTimeout"]);
        static int TIMEOUTBETWEENEVENTS = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultTimeoutBetweenEvents"]);

        public static IWebElement ElementToBeClickable(IWebElement element)
        {
            try
            {
                Thread.Sleep(TIMEOUTBETWEENEVENTS);
                return new WebDriverWait(DriverFactory.Instance, TimeSpan.FromSeconds(TIMEOUT)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
            }
            catch (Exception e)
            {
                DriverFactory.Instance.Quit();
                throw new Exception("Element not clickable. ", e.InnerException);
            }
        }
    }
}
