﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Helper;
using E_Commerce_Test_Automation.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace E_Commerce_Test_Automation.Pages
{
    public class ResultsPage: BasePage
    {
        #region Mapping screen elements
        private IWebElement Title => driver.FindElement(By.XPath("//h1[@class='search-title']"));
        #endregion

        #region Actions
        public void VerifyTitle(string value)
        {
            WaitUntil.ElementToBeClickable(Title);
            Assert.AreEqual(Title.Text, "Resultados de busca para \"" + value + "\"");
        }

        public void VerifyProduct(string value)
        {
            try
            {
                driver.FindElement(By.XPath("//span[text()='" + value + "']//ancestor::li//span[@class='int']"));
            }
            catch(Exception e)
            {
                DriverFactory.Instance.Quit();
                throw new Exception("Product is out of stock. ", e.InnerException);
            }
        }

        public void ClickOnProduct(string value)
        {
            driver.FindElement(By.XPath("//span[text()='" + value + "']")).Click();
        }
        #endregion
    }
}
