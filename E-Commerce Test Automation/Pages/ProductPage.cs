﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Helper;
using NUnit.Framework;
using OpenQA.Selenium;

namespace E_Commerce_Test_Automation.Pages
{
    public class ProductPage : BasePage
    {
        #region Mapping screen elements
        private IWebElement Title => driver.FindElement(By.XPath("//h1[@class='product-name']"));

        private IWebElement AddToCartButton => driver.FindElement(By.XPath("//button[@class='button-success button-pill right buy-button buy-button-product fluid']"));

        public IWebElement AddOneMoreItemButton => driver.FindElement(By.XPath("//a[@data-track='ClicouQuantidadeMais']"));

        public IWebElement Quantity => driver.FindElement(By.XPath("//div[@class='product-quantity-range']//span"));
        #endregion

        #region Actions
        public void VerifyTitle(string value)
        {
            WaitUntil.ElementToBeClickable(Title);
            Assert.AreEqual(Title.Text, value);
        }

        public void ClickOnAddToCartButton()
        {
            WaitUntil.ElementToBeClickable(AddToCartButton).Click();
        }

        public void VerifyQuantity(string quantity)
        {
            WaitUntil.ElementToBeClickable(Quantity);
            Assert.AreEqual(quantity, Quantity.Text);
        }

        public void ClickOnAddOneMoreItemButton()
        {
            WaitUntil.ElementToBeClickable(AddOneMoreItemButton).Click();
        }
        #endregion
    }
}
