﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Helper;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace E_Commerce_Test_Automation.Pages
{
    public class CartPage : BasePage
    {
        #region Mapping screen elements
        private IWebElement FinalizePurchaseButton => driver.FindElement(By.Id("btn-finalize-cart"));

        private IWebElement ContinueButton => driver.FindElement(By.XPath("//div[@class='steps-control-wrapper']/button"));

        private IWebElement CreditCardButton => driver.FindElement(By.XPath("//span[text()='Cartão de crédito']"));

        private IWebElement DeliveryAddressTitle => driver.FindElement(By.XPath("//h2[text()='Endereço de entrega']"));

        private IWebElement RemoveItemButton => driver.FindElement(By.XPath("//a[@class='link-trash']"));

        private IWebElement CartIsEmpty => driver.FindElement(By.XPath("//h2[text()='Seu carrinho está vazio']"));
        #endregion

        #region Actions
        public void VerifyTitlte()
        {
            Assert.IsTrue(driver.FindElement(By.XPath("//h2[contains(., 'Meu carrinho')]")).Displayed);
        }

        public void ClickOnFinalizePurchaseButton()
        {
            WaitUntil.ElementToBeClickable(FinalizePurchaseButton).Click();
        }

        public void ClickOnContinueButton()
        {
            Thread.Sleep(3000);
            WaitUntil.ElementToBeClickable(ContinueButton);
            Actions actions = new Actions(driver);
            actions.MoveToElement(ContinueButton).Click().Perform();
        }

        public void CLickOnCreditCardButton()
        {
            WaitUntil.ElementToBeClickable(CreditCardButton).Click();
        }

        public void VerifyDeliveryAddressTitle()
        {
            WaitUntil.ElementToBeClickable(DeliveryAddressTitle);
            Assert.IsTrue(DeliveryAddressTitle.Displayed);
        }

        public void ClickOnRemoveItemButton()
        {
            WaitUntil.ElementToBeClickable(RemoveItemButton).Click();
        }

        public void VerifyCartIsEmpty()
        {
            WaitUntil.ElementToBeClickable(CartIsEmpty);
            Assert.IsTrue(CartIsEmpty.Displayed);
        }
        #endregion
    }
}
