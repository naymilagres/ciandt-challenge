﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Helper;
using OpenQA.Selenium;

namespace E_Commerce_Test_Automation.Pages
{
    public class LoginPage : BasePage
    {
        #region Mapping screen elements
        private IWebElement EmailField => driver.FindElement(By.Id("signinField"));

        private IWebElement PasswordField => driver.FindElement(By.Id("password"));

        private IWebElement SigninButton => driver.FindElement(By.Id("signinButtonSend"));
        #endregion

        #region Actions
        public void FillEmailField(string value)
        {
            driver.SwitchTo().Frame("iframeLogin");
            WaitUntil.ElementToBeClickable(EmailField).SendKeys(value);
        }

        public void FillPasswordField(string value)
        {
            WaitUntil.ElementToBeClickable(PasswordField).SendKeys(value);
        }

        public void ClickOnSigninButton()
        {
            WaitUntil.ElementToBeClickable(SigninButton).Click();
            driver.SwitchTo().DefaultContent();
        }
        #endregion
    }
}
