﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Helper;
using OpenQA.Selenium;

namespace E_Commerce_Test_Automation.Pages
{
    public class HeaderPage : BasePage
    {
        #region Mapping screen elements
        private IWebElement SearchField => driver.FindElement(By.Id("suggestion-search"));

        private IWebElement SearchButton => driver.FindElement(By.XPath("//button[@class='search-icon-topbar wm-icon icon-search-topbar']"));

        private IWebElement SigninButton => driver.FindElement(By.Id("topbar-login-link"));

        private IWebElement CartButton => driver.FindElement(By.XPath("//a[@class='topbar-buttons open-link cart-link']"));
        #endregion

        #region Actions
        public void FillSearchField(string value)
        {
            WaitUntil.ElementToBeClickable(SearchField).SendKeys(value);
        }

        public void ClickOnSearchButton()
        {
            WaitUntil.ElementToBeClickable(SearchButton).Click();
        }

        public void ClickOnSigninButton()
        {
            WaitUntil.ElementToBeClickable(SigninButton).Click();
        }

        public void ClickOnCartButton()
        {
            WaitUntil.ElementToBeClickable(CartButton).Click();
        }
        #endregion
    }
}
