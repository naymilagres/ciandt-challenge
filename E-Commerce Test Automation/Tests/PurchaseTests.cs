﻿using E_Commerce_Test_Automation.Base;
using E_Commerce_Test_Automation.Pages;
using NUnit.Framework;
using System.Configuration;
using System.Threading;

namespace E_Commerce_Test_Automation.Tests
{
    public class PurchaseTests : BaseTest
    {
        #region Page object
        CartPage _cartPage;
        HeaderPage _headerPage;
        LoginPage _loginPage;
        ProductPage _productPage;
        ResultsPage _resultsPage;
        #endregion

        public void LogIn()
        {
            #region Page Instances
            _headerPage = new HeaderPage();
            _loginPage = new LoginPage();
            #endregion

            string username = ConfigurationManager.AppSettings["Username"];
            string password = ConfigurationManager.AppSettings["Password"];

            _headerPage.ClickOnSigninButton();

            _loginPage.FillEmailField(username);
            _loginPage.FillPasswordField(password);
            _loginPage.ClickOnSigninButton();
        }

        [Test]
        public void CustomerSelectsToBuyOneUnitOfTheProductChoosingCreditCard()
        {
            #region Page Instances
            _cartPage = new CartPage();
            _headerPage = new HeaderPage();
            _loginPage = new LoginPage();
            _productPage = new ProductPage();
            _resultsPage = new ResultsPage();
            #endregion

            LogIn();

            _headerPage.FillSearchField("Abridor de Latas para Canhotos Verona - Brinox");
            _headerPage.ClickOnSearchButton();

            _resultsPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _resultsPage.VerifyProduct("Abridor de Latas para Canhotos Verona - Brinox ");
            _resultsPage.ClickOnProduct("Abridor de Latas para Canhotos Verona - Brinox ");

            _productPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _productPage.VerifyQuantity("1");
            _productPage.ClickOnAddToCartButton();

            _headerPage.ClickOnCartButton();

            _cartPage.VerifyTitlte();
            _cartPage.ClickOnFinalizePurchaseButton();
            _cartPage.ClickOnContinueButton();
            _cartPage.VerifyDeliveryAddressTitle();
            _cartPage.ClickOnContinueButton();
            _cartPage.CLickOnCreditCardButton();
        }

        [Test]
        public void CustomerSelectsToBuyTwoUnitsOfTheProduct()
        {
            #region Page Instances
            _cartPage = new CartPage();
            _headerPage = new HeaderPage();
            _loginPage = new LoginPage();
            _productPage = new ProductPage();
            _resultsPage = new ResultsPage();
            #endregion

            LogIn();

            _headerPage.FillSearchField("Abridor de Latas para Canhotos Verona - Brinox");
            _headerPage.ClickOnSearchButton();

            _resultsPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _resultsPage.VerifyProduct("Abridor de Latas para Canhotos Verona - Brinox ");
            _resultsPage.ClickOnProduct("Abridor de Latas para Canhotos Verona - Brinox ");

            _productPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _productPage.VerifyQuantity("1");
            _productPage.ClickOnAddOneMoreItemButton();
            _productPage.VerifyQuantity("2");
            _productPage.ClickOnAddToCartButton();

            _headerPage.ClickOnCartButton();

            _cartPage.VerifyTitlte();
            _cartPage.ClickOnFinalizePurchaseButton();
            _cartPage.ClickOnContinueButton();
            _cartPage.VerifyDeliveryAddressTitle();
            _cartPage.ClickOnContinueButton();
            _cartPage.CLickOnCreditCardButton();
        }

        [Test]
        public void RemoveAnItemFromTheShoppingCart()
        {
            #region Page Instances
            _cartPage = new CartPage();
            _headerPage = new HeaderPage();
            _loginPage = new LoginPage();
            _productPage = new ProductPage();
            _resultsPage = new ResultsPage();
            #endregion

            LogIn();

            _headerPage.FillSearchField("Abridor de Latas para Canhotos Verona - Brinox");
            _headerPage.ClickOnSearchButton();

            _resultsPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _resultsPage.VerifyProduct("Abridor de Latas para Canhotos Verona - Brinox ");
            _resultsPage.ClickOnProduct("Abridor de Latas para Canhotos Verona - Brinox ");

            _productPage.VerifyTitle("Abridor de Latas para Canhotos Verona - Brinox");
            _productPage.VerifyQuantity("1");
            _productPage.ClickOnAddToCartButton();

            _headerPage.ClickOnCartButton();

            _cartPage.VerifyTitlte();
            _cartPage.ClickOnRemoveItemButton();
            _cartPage.VerifyCartIsEmpty();
        }
    }
}
