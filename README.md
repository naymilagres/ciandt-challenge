## CI&T - Automated Tests Challenge

This is a solution to automate the buying scenarios of an e-commerce site.
The automation was implemented using Selenium Webdriver and the programming language C#.
The chosen e-commerce website was https://www.walmart.com.br.

User created to run tests: 
E-mail: test-ciandt@mailinator.com
Password: 12345678

The URL, username, and password were parameterized in `App.config`.

### Architecture

    /E-Commerce Test Automation:		

        Base/                           # Super classes for Pages and Tests.      
            BasePage.cs             
            BaseTest.cs     

        Helper/                         # Used to keep useful methods for common Selenium operations          
            DriverFactory.cs
            WaitUntil.cs
        
        Page/                           # Folder where classes that implement pages mapping according to PageObjects.
            CartPage.cs
            HeaderPage.cs
            LoginPage.cs
            ProductPage.cs
            ResultsPage.cs

        Test/                            # Folder where test cases are kept.
            PurchaseTests.cs

        App.config                       # File where all variables needed to run the tests are kept.
        packages.config                  # File with the list of packages referenced by the project.

